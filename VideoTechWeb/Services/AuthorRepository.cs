using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VideoTech.Controllers;

namespace VideoTech.Services
{
    public class AuthorRepository
    {
        public IList<Author> GetAuthors()
        {
            var list = new List<Author>();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var cmd = new SqlCommand("SELECT * FROM Author", connection))
                {
                    connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var authorID = reader["AuthorID"].ToString();
                            var firstName = reader["FirstName"].ToString();
                            var lastName = reader["LastName"].ToString();

                            int authorIDInt;
                            int.TryParse(authorID, out authorIDInt);

                            list.Add(new Author { AuthorID = authorIDInt, FirstName = firstName, LastName = lastName });
                        }
                    }
                }
            }
            return list;
        }
    }

    [TestClass]
    public class AuthorRepositoryTest
    {
        [TestMethod]
        public void FirstTest()
        {
            Assert.AreEqual(2, 2);
        }
    }
}