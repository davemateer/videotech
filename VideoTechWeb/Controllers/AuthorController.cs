﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using VideoTech.Services;

namespace VideoTech.Controllers
{
    public class Author
    {
        public int AuthorID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class AuthorController : Controller
    {
        public ActionResult Index()
        {
            var repo = new AuthorRepository();
            return View(repo.GetAuthors());
        }
    }
}